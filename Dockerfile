# Use latest jboss/base-jdk:8 image as the base
FROM jboss/base

# Set the WILDFLY_VERSION env variable
ENV WILDFLY_VERSION 13.0.0.Final
ENV WILDFLY_SHA1 3d63b72d9479fea0e3462264dd2250ccd96435f9
ENV JBOSS_HOME /opt/jboss/wildfly

USER root

COPY jdk-8u181-linux-x64.rpm jdk-8u181-linux-x64.rpm
# Add the WildFly distribution to /opt, and make wildfly the owner of the extracted tar content
# Make sure the distribution is available from a well-known place
RUN rpm -Uvh jdk-8u181-linux-x64.rpm \
	&& cd $HOME \
    && curl -O https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz \
    && sha1sum wildfly-$WILDFLY_VERSION.tar.gz | grep $WILDFLY_SHA1 \
    && tar xf wildfly-$WILDFLY_VERSION.tar.gz \
    && mv $HOME/wildfly-$WILDFLY_VERSION $JBOSS_HOME \
    && rm wildfly-$WILDFLY_VERSION.tar.gz \
    && chown -R jboss:0 ${JBOSS_HOME} \
    && chmod -R g+rw ${JBOSS_HOME} \
    &&  /opt/jboss/wildfly/bin/add-user.sh admin Jboss@admin01 --silent   

# Ensure signals are forwarded to the JVM process correctly for graceful shutdown
ENV LAUNCH_JBOSS_IN_BACKGROUND true

USER jboss

# Expose the ports we're interested in
EXPOSE 8080 9990 8787


# Set the default command to run on boot
# This will boot WildFly in the standalone mode and bind to all interface
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0", "--debug"]

